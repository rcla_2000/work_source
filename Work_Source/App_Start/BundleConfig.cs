﻿using System.Web.Optimization;

namespace Work_Source
{
    public class BundleConfig
    {

        public static void RegisterBundles(BundleCollection bundles)
        {
      


                bundles.Add(new StyleBundle("~/bundles/css")
                .Include(
                "~/Content/css/bootstrap.min.css",
                "~/Content/css/bootstrap-multiselect.css",
                "~/Content/css/normalize.min.css",
                "~/Content/css/jquery.fancybox.css",
                "~/Content/css/flexslider.css",
                "~/Content/css/styles.css",
                "~/Content/css/queries.css",
                "~/Content/css/etline-font.css",
                "~/Content/bower_components/animate.css/animate.min.css"));

            
            bundles.Add(new ScriptBundle("~/bundles/js")
                .Include(
                "~/Content/js/jquery.js",
                "~/Content/js/bootstrap-multiselect.js",
                "~/Content/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js",
                "~/Content/bower_components/retina.js/dist/retina.js",
                "~/Content/js/jquery.fancybox.pack.js",
                "~/Content/js/scripts.js",
                "~/Content/js/jquery.flexslider-min.js",
                "~/Content/bower_components/classie/classie.js",
                "~/Content/bower_components/jquery-waypoints/lib/jquery.waypoints.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jsval")
                .Include("~/Content/js/jquery.validate*"));

        }

    }
}