﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Work_Source.Models;
/// <summary>
/// Descripción breve de PonderacionCriteriosModel
/// </summary>
public class PonderacionCriteriosModel:AbstractModel<Ponderacion_Criterios>
{
    public PonderacionCriteriosModel()
    {
        //
        // TODO: Agregar aquí la lógica del constructor
        //
    }

    public List<Ponderacion_Criterios> ObtenerPonderaciones(String IdOferta)
    {
        var result = from p in ctx.Ponderacion_Criterios
                     where p.IdOferta == IdOferta
                     select p;
        return result.ToList();
    }

    public int ObtenerIdPonderacion(String idoferta, int idcriterio)
    {
        var result = (from p in ctx.Ponderacion_Criterios
                     where p.IdOferta == idoferta && p.IdCriterio == idcriterio
                     select p.Id_Ponderacion).FirstOrDefault();
        return result;
    }
}