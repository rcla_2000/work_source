﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Work_Source.Models
{
    public class OfertaEmpleoCompleta
    {
        public OfertaEmpleoCompleta()
        {
            InformacionOferta = new OfertasEmpleo();
            InformacionPerfilContratacion = new PerfilContratacion();
            InformacionPonderacionCriterios = new Ponderacion_Criterios();
        }

        public OfertasEmpleo InformacionOferta { get; set; }
        public PerfilContratacion InformacionPerfilContratacion { get; set; }
        public Ponderacion_Criterios InformacionPonderacionCriterios { get; set; }

    }
}