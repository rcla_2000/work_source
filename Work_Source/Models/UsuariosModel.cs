﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Work_Source.Models;
/// <summary>
/// Descripción breve de UsuariosModel
/// </summary>

namespace Work_Source.Models
{ 

    public class UsuariosModel:AbstractModel<Usuarios>
    {
        public UsuariosModel()
        {
            //
            // TODO: Agregar aquí la lógica del constructor
            //

        }
        public int VerificarCuenta(String numeroCuenta, String pin)
        {
            pin = SecurityUtils.EncriptarSHA2(pin);
            var result = (from c in ctx.Usuarios
                          where c.codigo == numeroCuenta && c.password == pin
                          select c).Count();

            return result;
        }

        public string GenerarCodigo(string apellidos)
        {
            Random rnd = new Random();
            int numeroaleatorio = rnd.Next(10000000, 99999999);
            rnd.Next();
            string[] letras = apellidos.Split(' ');
            int total = letras.Length;
            string resultado = "";

            if (total == 1)
            {
                resultado += (letras[0].Substring(0, 1) + letras[0].Substring(0, 1)).ToUpper();
            }
            else
            {
                resultado += (letras[0].Substring(0, 1) + letras[1].Substring(0, 1)).ToUpper();
            }

            string code = resultado + numeroaleatorio.ToString();
            return code;
        }

        public IOrderedQueryable<Usuarios> Listar()
        {
            var result = from u in ctx.Usuarios
                         orderby u.apellidos
                         select u;
            return result;
        }

        public IOrderedQueryable<Usuarios> BuscarPorNombre(string dato)
        {
            var result = from u in ctx.Usuarios
                         where (u.nombres + " " + u.apellidos).Contains(dato)
                         orderby u.apellidos
                         select u;
            return result;
        }

    }
}