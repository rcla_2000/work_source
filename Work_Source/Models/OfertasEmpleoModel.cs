﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Work_Source.Models;
/// <summary>
/// Descripción breve de OfertasEmpleoModel
/// </summary>

namespace Work_Source.Models
{ 
    public class OfertasEmpleoModel:AbstractModel<OfertasEmpleo>
    {
        public OfertasEmpleoModel()
        {
            //
            // TODO: Agregar aquí la lógica del constructor
            //
        }

        public String CodigoOfertaEmpleo()
        {
            String codigo;
            var resultado = ctx.OfertasEmpleo
               .OrderByDescending(i => i.IdOferta).FirstOrDefault();

            if (resultado != null)
            {
                var r = resultado.IdOferta;
                var separador = r.Split('E');
                codigo = "OE" + (int.Parse(separador[1]) + 1).ToString("0000");
                return codigo;
            }
            else
            {
                codigo = "OE0001";
                return codigo;
            }
        }

        public List<OfertasEmpleo> OfertasUsuario(String codigo)
        {
            var misOfertas = from o in ctx.OfertasEmpleo
                             where o.creador == codigo
                             select o;
            return misOfertas.ToList();
        }

        public IOrderedQueryable<OfertasEmpleo> Listar()
        {
            var result = from o in ctx.OfertasEmpleo
                         orderby o.fecha_inicio descending
                         select o;
            return result;
        }

        public List<Usuarios> CreadoresDeOfertas()
        {
            var creadores = (from c in ctx.Usuarios
                             let nombreCompleto = string.Concat(c.nombres, " ", c.apellidos)
                             join o in ctx.OfertasEmpleo
                             on c.codigo equals o.creador
                             select c).Distinct(); 

            return creadores.ToList();
        }

        public IOrderedQueryable<OfertasEmpleo> OfertasPublicas()
        {
            var ofertas = from o in ctx.OfertasEmpleo
                          where o.fecha_inicio <= DateTime.Now
                          orderby o.fecha_inicio descending
                          select o;
            return ofertas;
        }

        public OfertaEmpleoCompleta ObtenerOfertaCompleta(OfertasEmpleo oferta)
        {
            PerfilContratacionModel perfilModel = new PerfilContratacionModel();
            OfertasEmpleo o = oferta;

            int idPerfil = (from perfil in ctx.PerfilContratacion
                           where perfil.IdOferta == o.IdOferta
                           select perfil.IdPerfil).FirstOrDefault();


            PerfilContratacion p = perfilModel.GetById(idPerfil);

            OfertaEmpleoCompleta ofertaCompleta = new OfertaEmpleoCompleta();

            ofertaCompleta.InformacionOferta.IdOferta = o.IdOferta;
                ofertaCompleta.InformacionOferta.NombreOferta = o.NombreOferta;
                ofertaCompleta.InformacionOferta.creador = o.creador;
                ofertaCompleta.InformacionOferta.estado = o.estado;
                ofertaCompleta.InformacionOferta.NombrePuesto = o.NombrePuesto;
                ofertaCompleta.InformacionOferta.MisionPuesto = o.MisionPuesto;
                ofertaCompleta.InformacionOferta.FuncionesBasicas = o.FuncionesBasicas;
                ofertaCompleta.InformacionOferta.AspectosVarios = o.AspectosVarios;
                ofertaCompleta.InformacionOferta.TipoContratacion = o.TipoContratacion;
                ofertaCompleta.InformacionOferta.NumPlazas = o.NumPlazas;
                ofertaCompleta.InformacionOferta.salario = o.salario;
                ofertaCompleta.InformacionOferta.fecha_inicio = o.fecha_inicio;
                ofertaCompleta.InformacionOferta.fecha_fin = o.fecha_fin;
                ofertaCompleta.InformacionOferta.detalles = o.detalles;
                ofertaCompleta.InformacionPerfilContratacion.IdPerfil = p.IdPerfil;
                ofertaCompleta.InformacionPerfilContratacion.IdOferta = p.IdOferta;
                ofertaCompleta.InformacionPerfilContratacion.GradoAcademico = p.GradoAcademico;
                ofertaCompleta.InformacionPerfilContratacion.Especialidad = p.Especialidad;
                ofertaCompleta.InformacionPerfilContratacion.NivelExperiencia = p.NivelExperiencia;
                ofertaCompleta.InformacionPerfilContratacion.Genero = p.Genero;
                ofertaCompleta.InformacionPerfilContratacion.ConocimientosEspecificos = p.ConocimientosEspecificos;
                ofertaCompleta.InformacionPerfilContratacion.Idiomas = p.Idiomas;

                return ofertaCompleta;   
            
        }

    }
}