﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Work_Source.Models
{
    public class InstitucionesModel:AbstractModel<Institucion>
    {
        public InstitucionesModel()
        {
            //
            // TODO: Agregar aquí la lógica del constructor
            //
        }

        public String CodigoInstitucion()
        {
            String codigo;
            var resultado = ctx.Institucion
               .OrderByDescending(i => i.IdInstitucion).FirstOrDefault();

            if (resultado != null)
            {
                var r = resultado.IdInstitucion;
                var separador = r.Split('S');
                codigo = "INS" + (int.Parse(separador[1]) + 1).ToString("00000");
                return codigo;
            }
            else
            {
                codigo = "INS00001";
                return codigo;
            }

        }

        public IOrderedQueryable<Institucion> Listar()
        {
            var result = from i in ctx.Institucion
                         orderby i.nombre
                         select i;
            return result;
        }

        public IOrderedQueryable<Institucion> BuscarInstitucion(String val)
        {
            var instituciones = from i in ctx.Institucion where i.nombre.Contains(val)
                                orderby i.nombre select i;
            return instituciones;
        }

        public List<Institucion> InstitucionesLibres()
        {
            var idIns = from ins in ctx.empleado_institucion
                                where ins.Usuarios.tipo_usuario == 2
                                select ins.IdInstitucion;

            var result = from i in ctx.Institucion
                         where !idIns.Contains(i.IdInstitucion)
                         orderby i.nombre
                         select i;

            if(result.ToList().Count > 0)
            {
                return result.ToList();
            }
            else
            {
                List<Institucion> lista = new List<Institucion>();
                Institucion institucion = new Institucion
                {
                    IdInstitucion = "",
                    nombre = "Todas las instituciones ya poseen un administrador",
                    telefono = "",
                    direccion = ""
                };
                lista.Add(institucion);
                return lista;
            }
        }

        public List<Institucion> InstitucionesLibresPlus(string IdAdministrador)
        {
            var idIns = from ins in ctx.empleado_institucion
                        where ins.Usuarios.tipo_usuario == 2
                        select ins.IdInstitucion;

            var result = from i in ctx.Institucion
                         where !idIns.Contains(i.IdInstitucion) ||
                         i.empleado_institucion.FirstOrDefault().IdEmpleado == IdAdministrador
                         orderby i.nombre
                         select i;
            return result.ToList();
            
        }

    }
}