﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Work_Source.Models
{
    public class TipoUsuariosModel:AbstractModel<TipoUsuario>
    {
        public TipoUsuariosModel()
        {
            //Mi lógica xD
        }

        public List<TipoUsuario> ObtenerTipoUsuarios()
        {
            var usuarios = (from u in ctx.TipoUsuario
                            orderby u.IdTipoUsuario descending
                            select u).Take(3);
            return usuarios.ToList();
        }
    }
}