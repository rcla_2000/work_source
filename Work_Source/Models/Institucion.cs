//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Work_Source.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    
    public partial class Institucion
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Institucion()
        {
            this.empleado_institucion = new HashSet<empleado_institucion>();
        }

        [Display(Name = "ID Institución")]
        public string IdInstitucion { get; set; }
        [Display(Name = "Nombre")]
        [Required(ErrorMessage = "Indique un nombre para la institución")]
        public string nombre { get; set; }
        [Display(Name = "Teléfono")]
        [Required(ErrorMessage = "Digite un número de telefónico de la institución")]
        [RegularExpression("^(2|6|7)[0-9]{3}-[0-9]{4}$", ErrorMessage = "Ingrese un n° telefónico válido y con el formato (2345-8978)")]
        public string telefono { get; set; }
        [Display(Name = "Dirección")]
        [Required(ErrorMessage = "Ingrese la dirección de la institución")]
        public string direccion { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<empleado_institucion> empleado_institucion { get; set; }
    }
}
