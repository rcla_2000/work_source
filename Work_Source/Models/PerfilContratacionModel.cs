﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Work_Source.Models;
/// <summary>
/// Descripción breve de PerfilContratacionModel
/// </summary>
public class PerfilContratacionModel:AbstractModel<PerfilContratacion>
{
    public PerfilContratacionModel()
    {
        //
        // TODO: Agregar aquí la lógica del constructor
        //
    }

    public PerfilContratacion BuscarPerfil(String codigo)
    {
        var result = (from p in ctx.PerfilContratacion
                     where p.IdOferta == codigo
                     select p).FirstOrDefault();
        return result;

        
    }

    public int ObtenerCodigoPerfil(String idoferta)
    {
        var result = (from c in ctx.PerfilContratacion
                     where c.IdOferta == idoferta
                     select c.IdPerfil).FirstOrDefault();
        return result;
    }
}