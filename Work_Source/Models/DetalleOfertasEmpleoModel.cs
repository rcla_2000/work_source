﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Work_Source.Models;

/// <summary>
/// Descripción breve de DetalleOfertasEmpleoModel
/// </summary>
public class DetalleOfertasEmpleoModel:AbstractModel<DetalleOfertasEmpleo>
{
    public DetalleOfertasEmpleoModel()
    {
        //
        // TODO: Agregar aquí la lógica del constructor
        //
    }

    public int ComprobarAplicacionOferta(string idusuario, string idoferta)
    {
        try
        {
            var result = (from p in ctx.DetalleOfertasEmpleo
                          where p.Candidato == idusuario && p.IdOferta == idoferta
                          select p).Count();
            return result;
        }
        catch
        {
            return 0;
        }
        

        
    }
}