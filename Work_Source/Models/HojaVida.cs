//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Work_Source.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class HojaVida
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public HojaVida()
        {
            this.ExperienciaProfesional = new HashSet<ExperienciaProfesional>();
            this.FormacionAcademica = new HashSet<FormacionAcademica>();
            this.ReferenciasPersonales = new HashSet<ReferenciasPersonales>();
        }
    
        public string IdHojaVida { get; set; }
        public string IdUsuario { get; set; }
        public string aptitudes { get; set; }
        public string logros_laborales { get; set; }
        public string idiomas { get; set; }
        public decimal pretension_salarial { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ExperienciaProfesional> ExperienciaProfesional { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FormacionAcademica> FormacionAcademica { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ReferenciasPersonales> ReferenciasPersonales { get; set; }
        public virtual Usuarios Usuarios { get; set; }
    }
}
