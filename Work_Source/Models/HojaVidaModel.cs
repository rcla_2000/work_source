﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Work_Source.Models;

/// <summary>
/// Descripción breve de HojaVidaModel
/// </summary>
public class HojaVidaModel : AbstractModel<HojaVida>
{
    //BolsaTrabajoGOESEntities ctx = new BolsaTrabajoGOESEntities();
    public HojaVidaModel()
    {
        //
        // TODO: Agregar aquí la lógica del constructor
        //
    }
    public String CodigoHojadeVida()
    {
        String codigo;
        var resultado = ctx.HojaVida
           .OrderByDescending(i => i.IdHojaVida).FirstOrDefault();

        if (resultado != null)
        {
            var r = resultado.IdHojaVida;
            var separador = r.Split('V');
            int cod = 0;
            cod = int.Parse(separador[1]) + 1;
            string ceros = "";
            if(cod > 1)
            {
                ceros = "0000";
            }else if(cod > 10)
            {
                ceros = "000";
            }
            else
            {
                ceros = "00";
            }

            codigo = "HDV" + (cod).ToString(ceros);
            return codigo;
        }
        else
        {
            codigo = "HDV00001";
            return codigo;
        }

    }

    public String DevolverCodigodeHoja(String CodigoUsuario)
    {
            var consulta = 
            from hoja in ctx.HojaVida
            where ( hoja.IdUsuario== "MH95878565")
            select hoja.IdHojaVida;

        return consulta.GetType().ToString();
    }
}