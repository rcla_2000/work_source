﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Work_Source.Models
{
    public class Empleado_InstitucionModel:AbstractModel<empleado_institucion>
    {
        public Empleado_InstitucionModel()
        {
            //Mi lógica xD
        }

        public IQueryable<string> ComprobarAdministracion(string codigoAdministrador)
        {
            var result = from e in ctx.empleado_institucion
                         where e.Usuarios.tipo_usuario == 2
                         && e.Usuarios.codigo == codigoAdministrador
                         select e.IdInstitucion;
            return result;
        }

        public int ObtenerIdEnlace(string IdEmpleado)
        {
            var result = (from e in ctx.empleado_institucion
                          where e.IdEmpleado == IdEmpleado
                          select e.id).FirstOrDefault();
            return result;
        }
    }
}