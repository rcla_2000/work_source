﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Work_Source.Models;
using PagedList;

namespace Work_Source.Controllers
{
    public class OfertasController : Controller
    {
        OfertasEmpleoModel model = new OfertasEmpleoModel();
        PerfilContratacionModel perfilModel = new PerfilContratacionModel();
        PonderacionCriteriosModel ponderacionModel = new PonderacionCriteriosModel();
        CriteriosEvaluacionModel criteriosModel = new CriteriosEvaluacionModel();
        BolsaTrabajoGOESEntities ctx = new BolsaTrabajoGOESEntities();
        // GET: Ofertas
        public ViewResult Index(string filtroBusqueda, string busqueda,string busqueda2, int? page)
        {
            page = 1;
            var ofertas = model.Listar();

            if (!string.IsNullOrEmpty(filtroBusqueda) && !string.IsNullOrEmpty(busqueda))
            {
                switch (filtroBusqueda)
                {
                    case "Todos":
                        ofertas = model.Listar();
                        break;
                    case "Nombre":
                        ofertas = from o in ctx.OfertasEmpleo
                                    where o.NombreOferta.Contains(busqueda)
                                    orderby o.NombreOferta
                                    select o;
                        break;

                    case "Creador":
                        ofertas = from o in ctx.OfertasEmpleo
                                  where o.creador == busqueda
                                  orderby o.fecha_inicio descending
                                  select o;
                        break;

                    case "Estado":
                        int estado = int.Parse(busqueda);
                        ofertas = from o in ctx.OfertasEmpleo
                                  where o.estado == estado
                                  orderby o.fecha_inicio descending
                                  select o;
                        break;

                    case "Fechas":
                        if (!string.IsNullOrEmpty(busqueda2))
                        {
                            DateTime fechaI = DateTime.Parse(busqueda);
                            DateTime fechaF = DateTime.Parse(busqueda2);

                            ofertas = from o in ctx.OfertasEmpleo
                                      where o.fecha_inicio >= fechaI && o.fecha_inicio <= fechaF
                                      orderby o.fecha_inicio descending
                                      select o;
                        }
                        break;
                }
            }

            ViewBag.ValorOculto = filtroBusqueda;
            ViewBag.Busqueda = busqueda + "/" + busqueda2;
            ViewBag.ListadoCreadores = new SelectList(model.CreadoresDeOfertas(), "codigo", "nombres");

            int pageSize = 10;
            int pageNumber = (page ?? 1);

            if (ofertas.Count() == 0)
            {
                TempData["errorMessage"] = "No se encontraron resultados en su búsqueda";
            }

            return View(ofertas.ToPagedList(pageNumber, pageSize));
        }

        // GET: Ofertas/Details/5
        public ActionResult Details(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                TempData["errorMessage"] = "Seleccione la oferta de la cual desea visualizar detalles";
                return RedirectToAction("Index");
            }

            OfertasEmpleo o = model.GetById(id);

            if(o == null)
            {
                TempData["errorMessage"] = "No se encontró el registro de la oferta que solicitó";
                return RedirectToAction("Index");
            }

            return View(o);
        }

        // GET: Ofertas/Create
        public ActionResult Create()
        {
            ViewBag.Criterios = criteriosModel.List();
            return View();
        }

        // POST: Ofertas/Create
        [HttpPost]
        public ActionResult Create(OfertaEmpleoCompleta oferta, string[] criterios)
        {
            int result1 = 0;
            int result2 = 0;
            int result3 = 0;
            ViewBag.Criterios = criteriosModel.List();
            try
            {
                if (ModelState.IsValid)
                {
                    if((decimal.Parse(criterios[0]) + decimal.Parse(criterios[1]) + decimal.Parse(criterios[2]) + decimal.Parse(criterios[3])) != 100)
                    {
                        ViewBag.ErrorPonderacion = "La suma de las ponderaciones de los criterios debe ser igual al 100%";
                        return View(oferta);
                    }

                    if(oferta.InformacionOferta.fecha_fin <= oferta.InformacionOferta.fecha_inicio)
                    {
                        ViewBag.ErrorFechas = "La Fecha de finalización debe ser mayor a la fecha de paertura de la oferta";
                        return View(oferta);
                    }

                    //Completando datos de la clase OfertaEmpleo
                    oferta.InformacionOferta.IdOferta = model.CodigoOfertaEmpleo();
                    oferta.InformacionOferta.creador = "LA78906578";
                    result1 = model.Insert(oferta.InformacionOferta);

                    //Completando datos de la clase PerfilContratacion
                    oferta.InformacionPerfilContratacion.IdOferta = oferta.InformacionOferta.IdOferta;
                    result2 = perfilModel.Insert(oferta.InformacionPerfilContratacion);

                    //Completando información de la clase Ponderacion_Criterios

                    Ponderacion_Criterios criterio1 = new Ponderacion_Criterios
                    {
                        IdOferta = oferta.InformacionOferta.IdOferta,
                        IdCriterio = 1,
                        Ponderacion = decimal.Parse(criterios[0])
                    };

                    Ponderacion_Criterios criterio2 = new Ponderacion_Criterios
                    {
                        IdOferta = oferta.InformacionOferta.IdOferta,
                        IdCriterio = 2,
                        Ponderacion = decimal.Parse(criterios[1])
                    };

                    Ponderacion_Criterios criterio3 = new Ponderacion_Criterios
                    {
                        IdOferta = oferta.InformacionOferta.IdOferta,
                        IdCriterio = 3,
                        Ponderacion = decimal.Parse(criterios[2])
                    };

                    Ponderacion_Criterios criterio4 = new Ponderacion_Criterios
                    {
                        IdOferta = oferta.InformacionOferta.IdOferta,
                        IdCriterio = 4,
                        Ponderacion = decimal.Parse(criterios[3])
                    };

                    result3 += ponderacionModel.Insert(criterio1);
                    result3 += ponderacionModel.Insert(criterio2);
                    result3 += ponderacionModel.Insert(criterio3);
                    result3 += ponderacionModel.Insert(criterio4);

                    if ((result1 > 0) && (result2 > 0) && (result3 == 4))
                    {
                        TempData["successMessage"] = "Oferta laboral agregada de forma exitosa";
                        return RedirectToAction("Index");
                    }
                    else { 
                        TempData["errorMessage"] = "Upps! Ocurrió un error al agregar la nueva oferta laboral";
                    }
                }
                else
                {
                    TempData["errorMessage"] = "algun dato erróneo";
                }

                return View(oferta);
            }
            catch (Exception e)
            {
                TempData["errorMessage"] = e.Message.ToString();
                return View(oferta);
            }
        }

        // GET: Ofertas/Edit/5
        public ActionResult Edit(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                TempData["errorMessage"] = "Para modificar la información de una oferta de empleo selecciónela del listado";
                return RedirectToAction("Index");
            }

            OfertasEmpleo oferta = model.GetById(id);

            if(oferta == null)
            {
                TempData["errorMessage"] = "No se encontró la oferta de empleo que solicitó";
                return RedirectToAction("Index");
            }

            OfertaEmpleoCompleta ofertaCompleta = model.ObtenerOfertaCompleta(oferta);
            ViewBag.Criterios = ponderacionModel.ObtenerPonderaciones(id);
            return View(ofertaCompleta);
        }

        // POST: Ofertas/Edit/5
        [HttpPost]
        public ActionResult Edit(OfertaEmpleoCompleta oferta, string[] criterios)
        {
            int result1 = 0;
            int result2 = 0;
            int result3 = 0;
            
            try
            {
                if (ModelState.IsValid)
                {
                    if ((decimal.Parse(criterios[0]) + decimal.Parse(criterios[1]) + decimal.Parse(criterios[2]) + decimal.Parse(criterios[3])) != 100)
                    {
                        ViewBag.ErrorPonderacion = "La suma de las ponderaciones de los criterios debe ser igual al 100%";
                        return View(oferta);
                    }

                    if (oferta.InformacionOferta.fecha_fin <= oferta.InformacionOferta.fecha_inicio)
                    {
                        ViewBag.ErrorFechas = "La Fecha de finalización debe ser mayor a la fecha de paertura de la oferta";
                        return View(oferta);
                    }
                    //Creando objetos Ponderacion_Criterios para actualizar la información en la BDD
                    Ponderacion_Criterios criterio1 = new Ponderacion_Criterios
                    {
                        Id_Ponderacion = ponderacionModel.ObtenerIdPonderacion(oferta.InformacionOferta.IdOferta, 1),
                        IdOferta = oferta.InformacionOferta.IdOferta,
                        IdCriterio = 1,
                        Ponderacion = decimal.Parse(criterios[0])
                    };

                    Ponderacion_Criterios criterio2 = new Ponderacion_Criterios
                    {
                        Id_Ponderacion = ponderacionModel.ObtenerIdPonderacion(oferta.InformacionOferta.IdOferta, 2),
                        IdOferta = oferta.InformacionOferta.IdOferta,
                        IdCriterio = 2,
                        Ponderacion = decimal.Parse(criterios[1])
                    };

                    Ponderacion_Criterios criterio3 = new Ponderacion_Criterios
                    {
                        Id_Ponderacion = ponderacionModel.ObtenerIdPonderacion(oferta.InformacionOferta.IdOferta, 3),
                        IdOferta = oferta.InformacionOferta.IdOferta,
                        IdCriterio = 3,
                        Ponderacion = decimal.Parse(criterios[2])
                    };

                    Ponderacion_Criterios criterio4 = new Ponderacion_Criterios
                    {
                        Id_Ponderacion = ponderacionModel.ObtenerIdPonderacion(oferta.InformacionOferta.IdOferta, 4),
                        IdOferta = oferta.InformacionOferta.IdOferta,
                        IdCriterio = 4,
                        Ponderacion = decimal.Parse(criterios[3])
                    };

                    result1 = model.Update(oferta.InformacionOferta, oferta.InformacionOferta.IdOferta);

                    if(result1 > 0)
                    {
                        result2 = perfilModel.Update(oferta.InformacionPerfilContratacion, oferta.InformacionPerfilContratacion.IdPerfil);
                    }
                    
                    if(result2 > 0)
                    {
                        result3 += ponderacionModel.Update(criterio1, criterio1.Id_Ponderacion);
                        result3 += ponderacionModel.Update(criterio2, criterio2.Id_Ponderacion);
                        result3 += ponderacionModel.Update(criterio3, criterio3.Id_Ponderacion);
                        result3 += ponderacionModel.Update(criterio4, criterio4.Id_Ponderacion);
                    }

                    if (result3 == 4)
                    {
                        TempData["successMessage"] = "Oferta laboral agregada de forma exitosa";
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = "Upps! Ocurrió un error al agregar la nueva oferta laboral";
                   
                }
                else
                {
                    TempData["errorMessage"] = "algun dato erróneo";
                }

                return View(oferta);
            }
            catch (Exception e)
            {
                TempData["errorMessage"] = e.Message.ToString();
                return View(oferta);
            }
        }

        // GET: Ofertas/Delete/5
        public ActionResult Delete(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                TempData["errorMessage"] = "Especifique la oferta laboral que desea eliminar de los registros";
                return RedirectToAction("Index");
            }

            OfertasEmpleo oferta = model.GetById(id);

            if(oferta == null)
            {
                TempData["errorMessage"] = "No se encontró el registro de la oferta de empleo que solicitó";
                return RedirectToAction("Index");
            }

            if(model.Remove(id) > 0)
            {
                TempData["successMessage"] = "Registro de oferta laboral eliminado de forma exitosa";
            }
            else
            {
                TempData["errorMessage"] = "No se pudo eliminar la oferta laboral que solicitó, puesto que posee aplicantes";
            }

            return RedirectToAction("Index");
        }

        public ActionResult VerOfertas(string txtBuscar, string estadoOferta, int? page)
        {
            var ofertas = model.OfertasPublicas();
            int estado;

            if (!string.IsNullOrEmpty(txtBuscar) && !string.IsNullOrEmpty(estadoOferta))
            {
                estado = int.Parse(estadoOferta);
                ofertas = from o in ctx.OfertasEmpleo
                          where (o.fecha_inicio <= DateTime.Now)
                          && (o.NombreOferta.Contains(txtBuscar))
                          && (o.estado == estado)
                          orderby o.fecha_inicio descending
                          select o;

            }
            else if (!string.IsNullOrEmpty(txtBuscar))
            {
                ofertas = from o in ctx.OfertasEmpleo
                          where (o.fecha_inicio <= DateTime.Now)
                          && (o.NombreOferta.Contains(txtBuscar))
                          orderby o.fecha_inicio descending
                          select o;
            }
            else if (!string.IsNullOrEmpty(estadoOferta))
            {
                estado = int.Parse(estadoOferta);
                ofertas = from o in ctx.OfertasEmpleo
                          where (o.fecha_inicio <= DateTime.Now)
                          && (o.estado == estado)
                          orderby o.fecha_inicio descending
                          select o;
            }

            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(ofertas.ToPagedList(pageNumber, pageSize));
        }

        public ActionResult AplicarOferta(string id)
        {
            DetalleOfertasEmpleoModel detalleModel = new DetalleOfertasEmpleoModel();
            if (string.IsNullOrEmpty(id))
            {
                TempData["errorMessage"] = "No se ha seleccionado ninguna oferta";
                return RedirectToAction("VerOfertas");
            }

            OfertasEmpleo o = model.GetById(id);

            if(o == null)
            {
                TempData["errorMessage"] = "No se encontró la oferta que solicitó";
                return RedirectToAction("VerOfertas");
            }

            if(detalleModel.ComprobarAplicacionOferta("LA78906578", o.IdOferta) > 0)
            {
                TempData["errorMessage"] = "Usted ya ha aplicado a ésta oferta";
                return RedirectToAction("VerOfertas");
            }

            DetalleOfertasEmpleo nuevoDetalle = new DetalleOfertasEmpleo
            {
                IdOferta = o.IdOferta,
                Candidato = "LA78906578",
                Criterio1 = 0,
                EstadoCriterio1 = 0,
                Criterio2 = 0,
                EstadoCriterio2 = 0,
                Criterio3 = 0,
                EstadoCriterio3 = 0,
                Criterio4 = 0,
                EstadoCriterio4 = 0,
                Decision = 0
            };

            if(detalleModel.Insert(nuevoDetalle) > 0)
            {
                TempData["successMessage"] = "Usted ha aplicado de manera satisfactoria a la oferta: " + o.NombreOferta;
                return RedirectToAction("VerOfertas");
            }

            return RedirectToAction("VerOfertas");

        }

        [Route("Ofertas/{IdUsuario}/{IdOferta}")]
        [HttpGet]
        public ActionResult Calificar(string IdUsuario, string IdOferta)
        {
            if (string.IsNullOrEmpty(IdUsuario) || string.IsNullOrEmpty(IdOferta))
            {
                TempData["errorMessage"] = "No se ha brindado la información completa para evaluar al usuario";
                return RedirectToAction("Index");
            }

            var user = from u in ctx.Usuarios
                       where u.codigo == IdUsuario
                       select u;

            ViewBag.NombreUsuario = user;

            return View();
        }

    }
}
