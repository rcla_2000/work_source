﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Work_Source.Models;
using PagedList;

namespace Work_Source.Controllers
{
    public class UsuariosController : Controller
    {
        UsuariosModel userModel = new UsuariosModel();
        TipoUsuariosModel tUserModel = new TipoUsuariosModel();
        InstitucionesModel insModel = new InstitucionesModel();
        Empleado_InstitucionModel empleadoModel = new Empleado_InstitucionModel();
        // GET: Usuarios
        public ActionResult Index(string txtBuscar, string currentFilter, int? page)
        {
            if (!string.IsNullOrEmpty(txtBuscar))
            {
                page = 1;
            }
            else
            {
                txtBuscar = currentFilter;
            }

            ViewBag.CurrentFilter = txtBuscar;
            var usuarios = userModel.Listar();

            if (!string.IsNullOrEmpty(txtBuscar))
            {
                usuarios = userModel.BuscarPorNombre(txtBuscar);
            }

            if(usuarios.Count() == 0)
            {
                TempData["errorMessage"] = "No se encontraron resultados en la búsqueda";
            }

            int pageSize = 10;
            int pageNumber = (page ?? 1);

            return View(usuarios.ToPagedList(pageNumber, pageSize));
        }

        // GET: Usuarios/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Usuarios/Create
        public ActionResult Create()
        {
            
            ViewBag.TipoDeUsuarios = new SelectList(tUserModel.ObtenerTipoUsuarios(), "IdTipoUsuario", "tipo");
            ViewBag.InstitucionesLibres = new SelectList(insModel.InstitucionesLibres(), "IdInstitucion", "nombre");
            return View();
        }

        // POST: Usuarios/Create
        [HttpPost]
        public ActionResult Create(Usuarios u, string ddlInstitucion, string ddlEstado)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    //Completamos el objeto antes de insertarlo en la BDD
                    u.codigo = userModel.GenerarCodigo(u.apellidos);
                    string contra = u.password;
                    u.password = SecurityUtils.EncriptarSHA2(u.password);

                    //Creando objeto de empleado_institucion
                    empleado_institucion empleado = new empleado_institucion();
                    empleado.IdEmpleado = u.codigo;

                    if (userModel.Insert(u) == 1)
                    {
                        switch (u.tipo_usuario)
                        {
                            case 2:
                                if (string.IsNullOrEmpty(ddlInstitucion))
                                {
                                    TempData["errorMessage"] = "Todas las instituciones ya poseen un administrador, si desea agregar uno nuevo, elimina alguno de los actuales";
                                    return RedirectToAction("Index");
                                }
                                empleado.estado = 1;
                                empleado.IdInstitucion = ddlInstitucion;
                                break;

                            case 3:
                                empleado.estado = int.Parse(ddlEstado);
                                empleado.IdInstitucion = "INS00001"; //Esta la capturamos de la sesion del administrador de institucion
                                break;
                        }

                        if(u.tipo_usuario == 2 || u.tipo_usuario == 3)
                        {
                            if(empleadoModel.Insert(empleado) > 0)
                            {
                                TempData["successMessage"] = u.nombres + " " + u.apellidos + " se ha registrado de forma exitosa, por favor ingrese a su cuenta de correo electrónico para verificar su cuenta";
                                //ClaseCorreo nuevoCorreo = new ClaseCorreo(u.email, u.apellidos, u.nombres, u.codigo, contra);
                                return RedirectToAction("Index");
                            }
                            else
                            {
                                TempData["errorMessage"] = "error en la tabla enlace";
                            }
                        }

                        TempData["successMessage"] = u.nombres + " " + u.apellidos + " se ha registrado de forma exitosa, por favor ingrese a su cuenta de correo electrónico para verificar su cuenta";
                        //ClaseCorreo nuevoCorreo2 = new ClaseCorreo(u.email, u.apellidos, u.nombres, u.codigo, contra);
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        TempData["errorMessage"] = "Error en la inserción de usuario";
                    }

                }
                else { 
                TempData["errorMessage"] = "Upps no se completó su regitro. Disculpe la molestia ha ocurrido un error en nuestros servidores";
                }

                ViewBag.TipoDeUsuarios = new SelectList(tUserModel.ObtenerTipoUsuarios(), "IdTipoUsuario", "tipo");
                ViewBag.InstitucionesLibres = new SelectList(insModel.InstitucionesLibres(), "IdInstitucion", "nombre");

                return View(u);
            }
            catch(Exception e)
            {
                TempData["errorMessage"] = "Ocurrió una excepción: " + e.Message.ToString();
                return View(u);
            }
        }

        // GET: Usuarios/Edit/5
        public ActionResult Edit(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                TempData["errorMessage"] = "Seleccione a un usuario para modificar su información";
                return RedirectToAction("Index");
            }

            Usuarios user = userModel.GetById(id);
            
            if(user == null)
            {
                TempData["errorMessage"] = "El registro de usuario que solicitó no se ha encontrado";
                return RedirectToAction("Index");
            }

            return View(user);
        }

        // POST: Usuarios/Edit/5
        [HttpPost]
        public ActionResult Edit(Usuarios u, string ddlEstado)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (userModel.Update(u, u.codigo) > 0)
                    {
                        if (u.tipo_usuario == 3)
                        {
                            var IdEnlace = empleadoModel.ObtenerIdEnlace(u.codigo);
                            empleado_institucion empleado = empleadoModel.GetById(IdEnlace);
                            empleado.estado = int.Parse(ddlEstado);
                            empleadoModel.Update(empleado, empleado.id);
                        }
                        TempData["successMessage"] = "Información de " + u.nombres + " " + u.apellidos + " actualizada de forma exitosa";
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        TempData["errorMessage"] = "No se pudo actualizar la información de: " + u.nombres + " " + u.apellidos;
                    }
                }
                else
                {
                    TempData["errorMessage"] = "Algún dato se encuentra erróneo, verifique la información que ingresó";
                }

                return View(u);
            }
            catch
            {
                return View(u);
            }
        }

        // GET: Usuarios/Delete/5
        public ActionResult Delete(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                TempData["errorMessage"] = "Seleccione al usuario que desea eliminar del listado de la tabla";
                return RedirectToAction("Index");
            }

            Usuarios user = userModel.GetById(id);

            if(user == null)
            {
                TempData["errorMessage"] = "No se encontró el registro del usuario que solicitó";
                return RedirectToAction("Index");
            }

            if(userModel.Remove(id) > 0)
            {
                TempData["successMessage"] = "El usuario ha sido eliminado de forma exitosa";
            }
            else
            {
                TempData["errorMessage"] = "No se pudo eliminar el registro del usuario que solicitó";
            }

            return RedirectToAction("Index");
        }

    }
}
