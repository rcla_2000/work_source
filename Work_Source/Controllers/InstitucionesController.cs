﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Work_Source.Models;
using PagedList;

namespace Work_Source.Controllers
{
    public class InstitucionesController : Controller
    {
        InstitucionesModel model = new InstitucionesModel();
        // GET: Instituciones
        public ActionResult Index(string txtBuscar, string currentFilter, int? page)
        {
            if (!string.IsNullOrEmpty(txtBuscar))
            {
                page = 1;
            }
            else
            {
                txtBuscar = currentFilter;
            }

            ViewBag.CurrentFilter = txtBuscar;

            var institucion = model.Listar();

            if (!string.IsNullOrEmpty(txtBuscar))
            {
                institucion = model.BuscarInstitucion(txtBuscar);
            }

            if(institucion.Count() == 0)
            {
                TempData["errorMessage"] = "No se encontró ningún registro de institución";
            }

            int pageSize = 10;
            int pageNumber = (page ?? 1);

            return View(institucion.ToPagedList(pageNumber, pageSize));
        }

        // GET: Instituciones/Details/5
        public ActionResult Details(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                TempData["errorMessage"] = "Seleccione una institución desde el listado para visualizar sus detalles";
                return RedirectToAction("Index");
            }

            Institucion i = model.GetById(id);
            
            if(i == null)
            {
                TempData["errorMessage"] = "No se encontró ningún registro de dicha institución";
                return RedirectToAction("Index");
            }

            return View(i);
        }

        // GET: Instituciones/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Instituciones/Create
        [HttpPost]
        public ActionResult Create(Institucion i)
        {
            try
            {
                // TODO: Add insert logic here
                if (ModelState.IsValid)
                {
                    i.IdInstitucion = model.CodigoInstitucion();
                    if(model.Insert(i) > 0)
                    {
                        TempData["successMessage"] = "Institución agregada de forma exitosa.";
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = "Upps ocurrió un error al agregar la institución, verifique la información ingresada";
                }

                return View(i);
            }
            catch
            {
                return View(i);
            }
        }

        // GET: Instituciones/Edit/5
        public ActionResult Edit(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                TempData["errorMessage"] = "Para editar una institución debe seleccionarla desde el listado de la tabla";
                return RedirectToAction("Index");

            }

            Institucion i = model.GetById(id);
            //Si no se encuentra el registro
            if (i == null)
            {
                TempData["errorMessage"] = "No se ha encontrado ningún registro de dicha institución. Seleccione una desde el listado de la tabla";
                return RedirectToAction("Index");

            }

            return View(i);
        }

        // POST: Instituciones/Edit/5
        [HttpPost]
        public ActionResult Edit(Institucion i)
        {
            try
            {
                // TODO: Add insert logic here
                if (ModelState.IsValid)
                {
                    if (model.Update(i, i.IdInstitucion) > 0)
                    {
                        TempData["successMessage"] = "Información de institución modificada exitosamente";
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = "Ocurrió un error al modificar la información de la institución verifique los datos ingresados";
                }
                return View(i);
            }
            catch
            {
                return View(i);
            }
        }

        // GET: Instituciones/Delete/5
        public ActionResult Delete(string id)
        {
            //Si se pasa vacìo
            if (string.IsNullOrEmpty(id))
            {
                TempData["errorMessage"] = "No ha seleccionado ningún registro de institución para eliminar";
                return RedirectToAction("Index");
                //return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Institucion i = model.GetById(id);
            //Si no existe
            if (i == null)
            {
                TempData["errorMessage"] = "El registro de institución que desea eliminar no existe";
                return RedirectToAction("Index");

            }

            if (model.Remove(id) > 0)
            {
                TempData["successMessage"] = "Institución Eliminada de forma exitosa";
            }
            else
            {
                TempData["errorMessage"] = "Upps! No se pudo eliminar la institución";
            }


            return RedirectToAction("Index");
        }

        
    }
}
