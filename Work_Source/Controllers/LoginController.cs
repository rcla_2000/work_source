﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Work_Source.Models;
using System.Web.Mvc;

namespace Work_Source.Controllers
{
    public class LoginController : Controller
    {
        UsuariosModel model = new UsuariosModel();
        BolsaTrabajoGOESEntities ctx = new BolsaTrabajoGOESEntities();

        // GET: Login
        public ActionResult IniciarSesion()
        {
            return View();

        }

        [HttpPost]
        public ActionResult IniciarSesion(Usuarios usuario)
        {
            try
            {
                // TODO: Add insert logic here
                if (ModelState.IsValid)
                {
                    String codigo = usuario.codigo;
                    string pass = SecurityUtils.EncriptarSHA2(usuario.password);

                    var resultado = (from c in ctx.Usuarios
                                    where c.codigo == codigo && c.password == pass
                                    select c).Count();


                    if (resultado.ToString() == "1")
                    {
                      

                        //Tipo de usuario
                        var tipo = from c in ctx.Usuarios
                                   where c.codigo == codigo
                                   select c.tipo_usuario;

                        int tipoUsuario = Convert.ToInt32(tipo.FirstOrDefault());

                        
                        if (tipoUsuario == 1) {
                            TempData["successMessage"] = "Bienvenido cliente";
                            //Redireccionar a la hoja de vida
                        }


                    }
                    else
                    {
                        TempData["errorMessage"] = "Verifique los datos ingresados";
                    }

                }
                return View(usuario);
            }
            catch
            {
                return View();
            }
        }



        //GET:Registrarse

        public ActionResult Registrarse()
        {
            return View();

        }


        [HttpPost]
        public ActionResult Registrarse(Usuarios usuario)
        {
            try
            {
                // TODO: Add insert logic here
                if (ModelState.IsValid)
                {
                    //Generamos el codigo
                    Random rnd = new Random();
                    int numeroaleatorio = rnd.Next(10000000, 99999999);
                    rnd.Next();
                    string[] letras = usuario.apellidos.Split(' ');
                    int total = letras.Length;
                    string resultado = "";
                    for (int i = 0; i < total; i++)
                    {
                        resultado += letras[i][0];
                    }
                    String code = resultado + numeroaleatorio.ToString();

                    //Creamos el código
                    usuario.codigo = code;
                    //usuario por defecto
                    usuario.tipo_usuario = 1;
                    string pass = usuario.password;

                    usuario.password = SecurityUtils.EncriptarSHA2(usuario.password);

                    if (model.Insert(usuario) > 0)
                    {
                        TempData["successMessage"] = "Su registro se completo exitosamente, por favor ingrese a su correo para verificar la cuenta";
                        ClaseCorreo nuevoCorreo = new ClaseCorreo(usuario.email, usuario.apellidos, usuario.nombres, code, pass);
                        return RedirectToAction("IniciarSesion");
                    }
                    TempData["errorMessage"] = "Lo sentimos, ha ocurrido un error en nuestros servidores";
                    
                }
                return View(usuario);
            }
            catch
            {
                return View();
            }
        }

    }
}